const numberOneElement = document.getElementById("number1");
console.log(numberOneElement);
const numberTwoElement = document.getElementById("number2");
console.log(numberTwoElement);
const nextButtonElement = document.getElementById("next-button1");
const infoContainerElement = document.getElementById("info-container");
const selectYourPlanElement = document.getElementById("select-your-plan-container");

const gobackElement = document.getElementById("goback-button");
const page2nextButtonElement = document.getElementById("next-button2");
const numberThreeElement = document.getElementById("number3");
console.log(numberThreeElement);
const pickAddOnElement = document.getElementById("add-ons-page-container");

const goback3Element = document.getElementById("goback-button-3");
const page3nextButtonElement = document.getElementById("next-button3");
const numberFourElement = document.getElementById("number4");
console.log(numberFourElement);

const goback4Element = document.getElementById("goback-button-4");
const page4nextButtonElement = document.getElementById("next-button4");

const finishContainerElement = document.getElementById("finish-container");
const thankyouContainerElement = document.getElementById("thank-you-container");

const number1Element = document.getElementById("number1-text");
const number2Element = document.getElementById("number2-text");
const number3Element = document.getElementById("number3-text");
const number4Element = document.getElementById("number4-text");


// signup form validation

const signupElement = document.getElementById("signup-form");
const nameElement = document.getElementById("firstname");
const nameErrorElement = document.getElementById("firstname-error");
const emailElement = document.getElementById("email");
const emailErrorElement = document.getElementById("email-error");
const numberElement = document.getElementById("number");
const numberErrorElement = document.getElementById("number-error");

function validateForm() {
    
    let isValid  = true

    if(nameElement.value.trim() === "") {
        nameErrorElement.textContent = "This field is required";
        isValid = false;
    }

    if(!isValidName(nameElement.value.trim())) {
        nameErrorElement.textContent = "This field is required";
        isValid = false;
    }

    if(emailElement.value.trim() === "") {
        emailErrorElement.textContent = "This field is required";
        isValid = false;
    } else if (!isValidEmail(emailElement.value.trim())) {
        emailErrorElement.textContent = "This field is required";
        isValid = false;
    }

    if(numberElement.value.trim() === "") {
        numberErrorElement.textContent = "This field is required";
        isValid = false;
    }

    if (numberElement.value.trim().length < 10) {
        numberErrorElement.textContent = "This field is required";
        isValid = false;
    }

    return isValid;
}

function isValidEmail(email) {
    const emailPattern = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return emailPattern.test(email);
}

function isValidName(name) {
    const NamePattern = /^[A-Za-z\s]+$/;
    return NamePattern.test(name);
}


function clearErrors() {
    nameErrorElement.textContent = "";
    emailErrorElement.textContent = "";
    numberErrorElement.textContent = "";
}


nextButtonElement.addEventListener("click", (e) => {
    e.preventDefault();
    clearErrors();
    if (!validateForm()) {
        return;
    }

    infoContainerElement.style.display = "none";
    numberOneElement.style.color = "#fff";
    numberOneElement.style.backgroundColor = "transparent";
    numberTwoElement.style.backgroundColor = "#BCDFFC";
    numberTwoElement.style.color = "black";
    numberOneElement.style.border = "1px solid #FFFFFF";
    selectYourPlanElement.style.display = "block";
    number1Element.style.color = "#fff";
    number2Element.style.color = "black";
    number1Element.style.backgroundColor = "transparent";
    number2Element.style.backgroundColor = "#BCDFFC"
    console.log("clicked");
})

thankyouContainerElement.style.display = "none";
pickAddOnElement.style.display = "none";
selectYourPlanElement.style.display = "none";
finishContainerElement.style.display = "none";

page4nextButtonElement.addEventListener("click", () => {
    finishContainerElement.style.display = "none";
    thankyouContainerElement.style.display = "block";
})

//toggle functionality

const toggleElement = document.getElementById("check");
toggleElement.addEventListener("change", () => {
    let beforeText = document.getElementsByClassName("monthly-price");
    //console.log(beforeText.length);
    let afterText = document.getElementsByClassName("annual-price");
    //console.log(afterText.length);
    const typeNameElements = document.querySelectorAll(".type-name");
    const monthlyTag = document.querySelector(".monthly-tag");
    const yearlyTag = document.querySelector(".yearly-tag");

    let freeElement = document.getElementsByClassName("free");
    for (let index = 0; index < beforeText.length; index++) {
        if(toggleElement.checked == true) {
            monthlyTag.style.color = "grey";
            yearlyTag.style.color = "black";
            beforeText[index].style.display = "none";
            afterText[index].style.display = "block"
            if (index <= 2) {
                freeElement[index].style.display = "block";
            }

        } else if(toggleElement.checked ==  false){
            yearlyTag.style.color = "grey";
            monthlyTag.style.color = "black";
            beforeText[index].style.display = "block";
            afterText[index].style.display = "none";
            if (index <= 2) {
                freeElement[index].style.display = "none";
            }
        }
    }
})

//2nd page type-of-plan highlighting
let isUserSelectPlan;
const typesOfPlans = document.getElementsByClassName("each-type-container");
for (let index = 0; index < typesOfPlans.length; index++) {
    typesOfPlans[index].addEventListener("click", () => {
        isUserSelectPlan = true;
        typesOfPlans[index].style.borderColor = "blue";
        typesOfPlans[index].style.backgroundColor = "#F8F9FE";
        for(let value = 0; value < typesOfPlans.length; value++) {
            if(value !== index) {
                typesOfPlans[value].style.borderColor = "grey";
                typesOfPlans[value].style.backgroundColor = "#FFFFFF";
            }
        }
    })
}

//page2 goback button functionlity

gobackElement.addEventListener("click", () => {
    selectYourPlanElement.style.display = "none";
    infoContainerElement.style.display = "block";
    numberOneElement.style.color = "#012956"
    numberTwoElement.style.color = "#FFFFFF"
    numberOneElement.style.backgroundColor = "#BCDFFC";
    numberTwoElement.style.backgroundColor = "transparent";
    number1Element.style.color = "black";
    number2Element.style.color = "#fff";
    number1Element.style.backgroundColor = "#BCDFFC";
    number2Element.style.backgroundColor = "transparent";
})

let selectPlanWarning = document.getElementById("select-plan-warning");

page2nextButtonElement.addEventListener("click", () => {
    if(!isUserSelectPlan) {
        selectPlanWarning.textContent = "Please select a plan."
        return;
    }
    numberThreeElement.style.color = "#012956"
    numberTwoElement.style.color = "#FFFFFF"
    selectPlanWarning.textContent = "";
    selectYourPlanElement.style.display = "none";
    numberThreeElement.style.backgroundColor = "#BCDFFC";
    numberTwoElement.style.backgroundColor = "transparent";
    pickAddOnElement.style.display = "block";
    number2Element.style.color = "#fff";
    number3Element.style.color = "black";
    number2Element.style.backgroundColor = "transparent";
    number3Element.style.backgroundColor = "#BCDFFC"
})

goback3Element.addEventListener("click", () => {
    numberTwoElement.style.color = "black"
    numberThreeElement.style.color = "#FFFFFF"
    pickAddOnElement.style.display = "none";
    selectYourPlanElement.style.display = "block";
    numberTwoElement.style.backgroundColor = "#BCDFFC";
    numberThreeElement.style.backgroundColor = "transparent";
    number3Element.style.color = "#fff";
    number2Element.style.color = "black";
    number3Element.style.backgroundColor = "transparent";
    number2Element.style.backgroundColor = "#BCDFFC"
})

page3nextButtonElement.addEventListener("click", () => {
    numberFourElement.style.color = "black"
    numberThreeElement.style.color = "#FFFFFF"
    numberThreeElement.style.backgroundColor = "transparent";
    numberFourElement.style.backgroundColor = "#BCDFFC";
    pickAddOnElement.style.display = "none";
    finishContainerElement.style.display = "block";
    number3Element.style.color = "#fff";
    number4Element.style.color = "black";
    number3Element.style.backgroundColor = "transparent";
    number4Element.style.backgroundColor = "#BCDFFC"
})

goback4Element.addEventListener("click", () => {
    numberThreeElement.style.color = "black"
    numberFourElement.style.color = "white"
    pickAddOnElement.style.display = "block";
    finishContainerElement.style.display = "none";
    numberFourElement.style.backgroundColor = "transparent";
    numberThreeElement.style.backgroundColor = "#BCDFFC";
    number4Element.style.color = "#fff";
    number3Element.style.color = "black";
    number4Element.style.backgroundColor = "transparent";
    number3Element.style.backgroundColor = "#BCDFFC"
})



// 3nd page type-of-pick highlighting

const checkboxes = document.querySelectorAll(".pick");

checkboxes.forEach((checkbox) => {
  checkbox.addEventListener("change", (event) => {
    const container = event.target.closest(".pick-type-container");
    if (event.target.checked) {
      container.style.borderColor = "blue";
      container.style.backgroundColor = "#F8F9FE";
    } else {
      container.style.borderColor = "grey";
      container.style.backgroundColor = "#FFFFFF";
    }
  });
});

// change button functionality

const changeElement = document.getElementById("change-plan");
changeElement.addEventListener("click", () => {
    finishContainerElement.style.display = "none";
    selectYourPlanElement.style.display = "block";
})

// total cost
const planContainers = document.querySelectorAll(".each-type-container");

let selectedPlan = null;

const selectedAddOns = document.querySelectorAll(".pick");
console.log(selectedAddOns);

function calculateTotalCost() {
    let totalCost = 0;

    // Calculate the cost of the selected plan
    if (selectedPlan) {
        const planPriceElement = toggleElement.checked
            ? selectedPlan.querySelector(".annual-price span")
            : selectedPlan.querySelector(".monthly-price span");
        const planPrice = parseInt(planPriceElement.textContent);
        totalCost += planPrice;
    }

    //Calculate the cost of selected add-ons
    selectedAddOns.forEach((addOn) => {
        //console.log(addOn);
        if (addOn.checked == true) {
            console.log(addOn);
            const container = addOn.closest(".pick-type-container");
            const addOnPriceElement = toggleElement.checked
                ? container.querySelector(".annual-price span")
                : container.querySelector(".monthly-price span");
            const addOnPrice = parseInt(addOnPriceElement.textContent);
            totalCost += addOnPrice;
        }
    });

    return totalCost;
}

// Function to update the total cost displayed
function updateTotalCost() {
    const totalCostElement = document.querySelector(".total-cost-heading");
    const totalPrice = document.querySelector(".total-cost-price");
    totalPrice.classList.add("total-price");
    const totalCost = calculateTotalCost();

    const totalPriceType = toggleElement.checked ? "Total(per year)" : "Total(per month)";
    const totalcostType = toggleElement.checked ? "/yr" : "/mo";


    totalCostElement.textContent = totalPriceType;
    totalCostElement.classList.add("add-on-type");
    totalPrice.textContent = `+$${totalCost.toFixed(2)}${totalcostType}`
}

// Function to update the selected plan display in the finishing container
function updateSelectedPlanFinishingDisplay() {
    //const selectedPlanContainer = document.getElementById("selected-plan");
    const selectedPlanType = document.querySelector(".selected-plan-type");
    const selectedPlanPrice = document.querySelector(".selected-plan-price");
    if (selectedPlan) {
        const planName = selectedPlan.querySelector(".type-name").textContent;
        const planPriceElement = toggleElement.checked
            ? selectedPlan.querySelector(".annual-price span")
            : selectedPlan.querySelector(".monthly-price span");
        const planPrice = parseInt(planPriceElement.textContent);
        const planBilling = toggleElement.checked ? "Yearly" : "Monthly";
        const planTag = toggleElement.checked ? "yr" : "mo";

        // selectedPlanContainer.innerHTML = `
        //   <p>${planName} (${planBilling})  $${planPrice.toFixed(2)}/${planTag}</p>
        // `;
        selectedPlanType.textContent = `${planName} (${planBilling})`
        selectedPlanPrice.textContent = `$${planPrice.toFixed(2)}/${planTag}`
    } else {
        selectedPlanContainer.innerHTML = "" ;
    }
}

// Function to update the selected add-ons display in the finishing container

function updateSelectedAddonsFinishingDisplay() {
    const selectedAddonContainer = document.getElementById("selected-addon-finishing");
    selectedAddonContainer.innerHTML = ""; 

    let selectedAddOns = document.querySelectorAll(".pick");

    selectedAddOns.forEach((addOn) => {
        if(addOn.checked == true) {
            //console.log(addOn);
            const container = addOn.closest(".pick-type-container");
            const addOnNameElement = container.querySelector("label");
            const serviceName = addOnNameElement.textContent;
            //console.log(serviceName);

            const addOnPriceElement = toggleElement.checked
                ? container.querySelector(".annual-price span")
                : container.querySelector(".monthly-price span");

                if (addOnPriceElement) {
                    const addOnPrice = parseInt(addOnPriceElement.textContent);
                    console.log(addOnPrice);
                    
                    const addOnContainer = document.createElement("div");
                    addOnContainer.classList.add("add-on-container");

                    const addOnType = document.createElement("h3");
                    addOnType.classList.add("add-on-type")
                    const addOnTypePrice = document.createElement("p");
                    addOnTypePrice.classList.add("add-on-type-price");

                    addOnContainer.append(addOnType);
                    addOnContainer.append(addOnTypePrice);

                    selectedAddonContainer.append(addOnContainer);

                    addOnType.textContent = `${serviceName}`;
                    addOnTypePrice.textContent = `+$${addOnPrice.toFixed(2)}/${toggleElement.checked ? 'yr' : 'mo'}`
                }
            
        }
        
    })

}


// Function to update the finishing content
function updateFinishingContent() {
    updateSelectedPlanFinishingDisplay();

    updateSelectedAddonsFinishingDisplay();
}

// Attach change event listeners for the plan and add-ons
planContainers.forEach((container) => {
    container.addEventListener("click", () => {
        planContainers.forEach((c) => {
            c.classList.remove("selected");
        });
        container.classList.add("selected");

        selectedPlan = container;

        updateTotalCost();

        updateFinishingContent();
    });
});

checkboxes.forEach((checkbox) => {
    checkbox.addEventListener("change", () => {
        updateTotalCost();

        updateFinishingContent();
    });
});

toggleElement.addEventListener("change", () => {
    updateTotalCost();

    updateFinishingContent();
});